package com.productions.unit55.projectmotivate.support;


import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


import org.jcodec.api.SequenceEncoder;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;


/**
 * Created by Brad on 10/11/2015.
 */
public class VideoGenerator{
    private DatabaseController dbConroller;
    private int height = 640;
    private int width = 480;
    int framesPerPic = 25;
/////////////////////////START////////////////////////////////////////
    public VideoGenerator(Context c) {
        try {
            File file = this.GetSDPathToFile("output.mp4");
            SequenceEncoder encoder = new SequenceEncoder(file);
            Picture p = null;
            dbConroller = new DatabaseController(c);
            SQLiteDatabase db = dbConroller.getReadableDatabase();

            //get file names from DB
            String[] projection = {
                    DBSchema._ID,
                    DBSchema.COLUMN_NAME_IMAGENAME,
            };

            String sortOrder =  DBSchema._ID + " ASC";

            Cursor cursor = db.query(
                    DBSchema.TABLE_NAME,  // The table to query
                    projection,                               // The columns to return
                    null,                                     // The columns for the WHERE clause
                    null,                                     // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );

            cursor.moveToFirst();

            String fileOne = " ";
            String fileTwo = " ";

            if (cursor.getCount() > 1) {
                for (int z = 0; z < cursor.getCount() - 1; z++) {
                    // getting bitmap from drawable paths
                    fileOne = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), "projectMotivate" + File.separator
                            + cursor.getString(1) + ".jpg").toString();

                    cursor.moveToNext();

                    fileTwo = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), "projectMotivate" + File.separator
                            + cursor.getString(1) + ".jpg").toString();

                    cursor.moveToPrevious();

                    File f1 = new File(fileOne);
                    File f2 = new File(fileTwo);

                    if (f1.exists() && f2.exists()) {
                        for (int j = 0; j < framesPerPic; j++) {
                            p = createPicture(fileOne, fileTwo, j);
                            encoder.encodeNativeFrame(p);
                        }
                    }

                    Log.i("super message", "z: " + z + "    Cursor Size " + cursor.getCount() + "     Cursor   location:  " + cursor.getPosition() + "   fileOne: " + fileOne + "   fileTwo: " + fileTwo);
                    cursor.moveToNext();
                }
            } else {
                fileOne = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), "projectMotivate" + File.separator
                        + cursor.getString(1) + ".jpg").toString();

                for (int j = 0; j < framesPerPic; j++) {
                    p = createPicture(fileOne, fileOne, j);
                    encoder.encodeNativeFrame(p);
                }
            }
            encoder.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Picture createPicture(String fileOne, String fileTwo, int index) {
        Picture pic = null;

        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        //create image one
        //BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmapOne = BitmapFactory.decodeFile(fileOne);
        bitmapOne = Bitmap.createScaledBitmap(bitmapOne,width,height,true);

        //Create image two
        //BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
        Bitmap bitmapTwo = BitmapFactory.decodeFile(fileTwo);
        bitmapTwo = Bitmap.createScaledBitmap(bitmapTwo,width,height,true);

        //Combine two images
        Canvas c = new Canvas(result);
        c.setBitmap(result);
        c.drawBitmap(bitmapOne, new Matrix(), null);

        Paint p = new Paint();
        double newIndex = Double.parseDouble(index + "");
        double numerator = newIndex/framesPerPic;
        double alphaDouble = numerator*255;
        int alpha = (int) alphaDouble;
        p.setAlpha(alpha);
        c.drawBitmap(bitmapTwo, 0, 0, p);

        pic = this.fromBitmap(result);


        return pic;
    }

// get full SD path
    File GetSDPathToFile(String fileName) {
        File direct = new File(Environment.getExternalStorageDirectory() + "/projectMotivate");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/projectMotivate/");
            wallpaperDirectory.mkdirs();
        }

        return new File(new File("/sdcard/projectMotivate/"), fileName);
    }

    // convert from Bitmap to Picture (jcodec native structure)
    public Picture fromBitmap(Bitmap src) {
        Picture dst = Picture.create(width, height, ColorSpace.RGB);
        fromBitmap(src, dst);
        return dst;
    }

    public void fromBitmap(Bitmap src, Picture dst) {
        int[] dstData = dst.getPlaneData(0);
        int[] packed = new int[width * height];

        src.getPixels(packed, 0, width, 0, 0, width, height);

        for (int i = 0, srcOff = 0, dstOff = 0; i < height; i++) {
            for (int j = 0; j < width; j++, srcOff++, dstOff += 3) {
                int rgb = packed[srcOff];
                dstData[dstOff]     = (rgb >> 16) & 0xff;
                dstData[dstOff + 1] = (rgb >> 8) & 0xff;
                dstData[dstOff + 2] = rgb & 0xff;
            }
        }
    }
/////////////////////////END///////////////////////////////////////
}
