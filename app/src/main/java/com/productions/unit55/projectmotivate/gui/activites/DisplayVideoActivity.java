package com.productions.unit55.projectmotivate.gui.activites;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.VideoView;

import com.productions.unit55.projectmotivate.R;

public class DisplayVideoActivity extends Activity {

    VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_video);

        vv = (VideoView) this.findViewById(R.id.videoView);
        Uri videoUri = Uri.parse(Environment.getExternalStorageDirectory() + "/projectMotivate/output.mp4");

        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        vv.setVideoURI(videoUri);
        vv.start();

    }
}
