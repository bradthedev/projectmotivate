package com.productions.unit55.projectmotivate.gui.activites;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.productions.unit55.projectmotivate.core.ProgressReport;
import com.productions.unit55.projectmotivate.gui.support.CameraPreview;
import com.productions.unit55.projectmotivate.R;
import com.productions.unit55.projectmotivate.support.DBSchema;
import com.productions.unit55.projectmotivate.support.DatabaseController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends Activity {
    private static String TAG = "Brad";
    private static String USERNAME = "superUSER";
    private static ProgressReport newReport;
    public static final int MEDIA_TYPE_IMAGE = 1;

    private static DatabaseController dbHandler;
    private Camera mCamera;
    private CameraPreview mPreview;
    private File picLocation;
    private Button takePicButton;
    private static ContentValues dbValues;
    
    private Button acceptPicButton;
    private Button rejectPicButton;

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d(TAG, "Error creating media file, check storage permissions: ");
                return;
            }
            picLocation = pictureFile;

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create an instance of Camera
        mCamera = getCameraInstance();
        //// Solution to ensure that image is in portrait mode
        Camera.Parameters p = mCamera.getParameters();

        p.set("orientation", "portrait");
        p.set("rotation", 90);
        mCamera.setParameters(p);

        takePicButton = (Button) findViewById(R.id.button_capture);
        acceptPicButton = (Button) findViewById(R.id.accept_picture_button);
        rejectPicButton = (Button) findViewById(R.id.reject_picture_button);

        dbValues = new ContentValues();
        dbHandler = new DatabaseController(this);
        startPreview();
    }

    public Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public void onTakePicture(View target) {

        takePicButton.setVisibility(View.GONE);
        acceptPicButton.setVisibility(View.VISIBLE);
        rejectPicButton.setVisibility(View.VISIBLE);

        mCamera.takePicture(null, null, mPicture);

    }

    public void onAcceptPicture(View target) {
        releaseCamera();
        Intent i = new Intent(getApplicationContext(), ProgressReportActivity.class);
        try {
            startActivity(i);
        } catch (Exception e) {
            System.out.println("-----super duper crazy fucking error---------" + e.getMessage());
        }
        finish();
    }

    public void onRejectPicture(View target) {
        picLocation.delete();

        takePicButton.setVisibility(View.VISIBLE);
        acceptPicButton.setVisibility(View.INVISIBLE);
        rejectPicButton.setVisibility(View.INVISIBLE);

        mCamera.startPreview();
    }

    private void startPreview() {
        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    //releasing the Camera
    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }


    //saving the pic

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "projectMotivate");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("projectMotivate", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        SimpleDateFormat theFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        theFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
        Date current = new Date();
        String timeStamp = theFormat.format(current);

        File mediaFile;
        String imageName = "PM_" + USERNAME + "_" + timeStamp;
        SQLiteDatabase db = dbHandler.getWritableDatabase();

        dbValues.put(DBSchema.COLUMN_NAME_IMAGENAME, imageName);

        try {
            dbValues.put(DBSchema.COLUMN_NAME_DATEIMAGETAKEN, theFormat.parse(timeStamp).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        long newRowID;
        newRowID = db.insert(DBSchema.TABLE_NAME, null, dbValues);


        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    imageName + ".jpg");
        }else {
            return null;
        }

        return mediaFile;
    }
}