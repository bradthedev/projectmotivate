package com.productions.unit55.projectmotivate.core;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Brad on 17/08/2015.
 */
public class ProgressReport implements Serializable {

    private String imageName;
    private Date dateImageTaken;
    private String weight;
    private String comment;

    public ProgressReport() {
        imageName = "";
        weight = "";
        comment = "";
        dateImageTaken = new Date();
    }

    public String getImageName() {
        return imageName;
    }

    public Date getDateImageTaken() {
        return dateImageTaken;
    }

    public String getWeight() {
        return weight;
    }

    public String getComment() {
        return comment;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setDateImageTaken(Date dateImageTaken) {
        this.dateImageTaken = dateImageTaken;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
