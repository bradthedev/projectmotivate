package com.productions.unit55.projectmotivate.support;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Brad on 25/08/2015.
 */
public class DatabaseController extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "motivate.db";

    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_DATE = " TEXT";
    private static final String TYPE_INT = " TEXT";
    private static final String COMMA_SEP = ",";

    //Database Methods
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DBSchema.TABLE_NAME + " (" +
                    DBSchema._ID + " INTEGER PRIMARY KEY," +
                    DBSchema.COLUMN_NAME_ENTRY_ID + TYPE_TEXT + COMMA_SEP +
                    DBSchema.COLUMN_NAME_IMAGENAME + TYPE_TEXT + COMMA_SEP +
                    DBSchema.COLUMN_NAME_DATEIMAGETAKEN + TYPE_DATE + COMMA_SEP +
                    DBSchema.COLUMN_NAME_WEIGHT + TYPE_INT + COMMA_SEP +
                    DBSchema.COLUMN_NAME_COMMENT + TYPE_TEXT +
    // Any other options for the CREATE command
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DBSchema.TABLE_NAME;

    /////////////////////////////////////

    public DatabaseController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }



    //The Database schema with variables to be used :)


}
