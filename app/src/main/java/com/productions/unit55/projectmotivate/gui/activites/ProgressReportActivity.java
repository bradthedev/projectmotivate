package com.productions.unit55.projectmotivate.gui.activites;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;

import com.productions.unit55.projectmotivate.R;
import com.productions.unit55.projectmotivate.support.DBSchema;
import com.productions.unit55.projectmotivate.support.DatabaseController;
import com.productions.unit55.projectmotivate.support.VideoGenerator;

import java.io.File;

/**
 * Created by Brad on 21/08/2015.
 */
public class ProgressReportActivity extends Activity {

    private ImageView newImage;
    private DatabaseController dbHandler;
    private String imgName;

    private DatabaseController dbController = new DatabaseController(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_report);

        dbHandler = new DatabaseController(this);

        newImage = (ImageView) findViewById(R.id.imageView);

        SQLiteDatabase db = dbHandler.getReadableDatabase();
        String[] projection = {
                DBSchema.COLUMN_NAME_IMAGENAME,
                DBSchema._ID
        };

        String sortOrder = DBSchema._ID + " DESC";

        Cursor c = db.query(DBSchema.TABLE_NAME, projection, null, null, null, null, sortOrder);
        c.moveToFirst();

        File imgFile = new  File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "projectMotivate" + File.separator
                + c.getString(0) + ".jpg");

        if(imgFile.exists()){
            imgName = imgFile.toString();
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.imageView);
            myImage.setImageBitmap(myBitmap);

        }

    }

    public void onGenerateClicked(View view) {
        SQLiteDatabase db = dbController.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBSchema.COLUMN_NAME_WEIGHT, findViewById(R.id.weightText).toString());
        values.put(DBSchema.COLUMN_NAME_COMMENT, findViewById(R.id.commentText).toString());

        // Which row to update, based on the ID
        String selection = DBSchema.COLUMN_NAME_IMAGENAME + " LIKE ?";
        String[] selectionArgs = {String.valueOf(imgName)};

        int count = db.update(
                DBSchema.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        VideoGenerator vg = new VideoGenerator(this);

        Intent i = new Intent(getApplicationContext(), DisplayVideoActivity.class);
        try {
            startActivity(i);
        } catch (Exception e) {
            System.out.println("-----super duper crazy fucking error---------" + e.getMessage());
        }
    }
}
