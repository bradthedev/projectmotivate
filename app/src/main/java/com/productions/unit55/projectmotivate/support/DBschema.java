package com.productions.unit55.projectmotivate.support;

import android.provider.BaseColumns;

/**
 * Created by Brad on 02/11/2015.
 * Git This
 */

public final class DBSchema implements BaseColumns {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public static final String TABLE_NAME = "motivateDB";
    public static final String COLUMN_NAME_ENTRY_ID = "recordID";
    public static final String COLUMN_NAME_IMAGENAME = "imageName";
    public static final String COLUMN_NAME_DATEIMAGETAKEN = "dateImageTaken";
    public static final String COLUMN_NAME_WEIGHT = "weight";
    public static final String COLUMN_NAME_COMMENT = "comment";

    public DBSchema() {}
}